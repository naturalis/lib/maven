FROM docker:27

ARG MAVEN_VERSION=3.9.9 # Default value provided

RUN wget -q -O "/etc/apk/keys/adoptium.rsa.pub" "https://packages.adoptium.net/artifactory/api/security/keypair/public/repositories/apk" && \
    echo 'https://packages.adoptium.net/artifactory/apk/alpine/main' >> /etc/apk/repositories && \
    apk add --no-cache temurin-21-jdk git curl && \
    apk upgrade busybox curl && \
    wget -o - --quiet "https://dlcdn.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz" && \
    wget -o - --quiet "https://downloads.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz.sha512" && \
    echo "$(cat apache-maven-${MAVEN_VERSION}-bin.tar.gz.sha512)  apache-maven-${MAVEN_VERSION}-bin.tar.gz" | sha512sum -c && \
    tar xzvf "apache-maven-${MAVEN_VERSION}-bin.tar.gz" && \
    mv "apache-maven-${MAVEN_VERSION}" /opt/. && \
    ln -s "/opt/apache-maven-${MAVEN_VERSION}" /opt/maven && \
    rm "apache-maven-${MAVEN_VERSION}-bin.tar.gz"*

ENV PATH=$PATH:/opt/maven/bin