# Maven Docker Image

Utility docker image build on the [official maven image](https://hub.docker.com/_/maven) and modified for building maven projects, creating new images and pushing the results to an image repository.


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`

